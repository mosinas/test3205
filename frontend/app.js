$(document).ready(function () {
  let host = location.host,
    videoId,
    videoWidth,
    videoHeight;

  $('form').submit(function (e) {

    e.preventDefault();
    $('.alert').fadeOut('slow').addClass('d-none');
    $('.video-content-iframe').append('').addClass('d-none');

    let request = $(this).find('input[type="text"]').val(),
        link =  location.protocol + '//' + location.host + '/video?url=' + request;

    if(!request) {
      return false;
    }

    $.get(link, function (response) {

      videoId = response.videoId;
      videoWidth = response.width;
      videoHeight = response.height;

      $('.video-content').find('.card-title').html(response.title);
      $('.video-content').find('.card-subtitle').html(response.author_name);

      $('.video-content').find('.card-img-top')
        .attr('src', response.thumbnail_url)
        .width(response.thumbnail_width)
        .height(response.thumbnail_height);

      $('.video-content').removeClass('d-none');
      $('.card').removeClass('d-none');
      $('.card-img-top').removeClass('d-none').show();

       $('.video-content-iframe').empty();
    }).catch(function (error) {
        $('.alert').fadeOut('slow').removeClass('d-none');
    });

  });

  $('.card-img-top').click(function (e) {
    $(this).fadeOut('slow').addClass('d-none');

    let iframe = '<iframe width="' + videoWidth +  '" height="' + videoHeight + '" src="//www.youtube.com/embed/' + videoId +'?autoplay=1" frameborder="0" allowfullscreen></iframe>';

    $('.video-content-iframe').append(iframe).removeClass('d-none');
  });

});
