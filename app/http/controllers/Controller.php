<?php


namespace App\Http\Controllers;

use App\services\video\AbstractVideoProvider;
use App\services\video\FabricVideoProvider;
use Symfony\Component\HttpFoundation\Request;

class Controller
{
    /**
     * @param Request $request
     * @return array
     * @throws \App\Exceptions\VideoProviderException
     */
    public function actionVideo(Request $request)
    {
        $url = $request->get('url');

        if(!filter_var($url, FILTER_VALIDATE_URL)) {
            return ['error' => 'url must be link'];
        }

        $fabricProvider = new FabricVideoProvider();

        return $fabricProvider->getVideoProvider($url)->getInfo();
    }

}
