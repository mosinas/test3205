<?php
    return [
        [
            'name' => 'video',
            'method' => ['GET'],
            'pattern' => 'video',
            'controller' => \App\Http\Controllers\Controller::class,
            'action' => 'actionVideo',
//            'requirements' => '\w+',
        ],
        [
            'name' => 'about',
            'method' => ['GET'],
            'pattern' => 'about',
            'controller' => \App\Http\Controllers\Controller::class,
            'action' => 'actionIndex',
            'methods' => 'GET',
        ],
    ];
