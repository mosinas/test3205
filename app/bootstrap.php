<?php

require_once __DIR__ . '/../vendor/autoload.php';

use App\Exceptions\VideoProviderException;
use \Symfony\Component\Routing\Route;
use \Symfony\Component\Routing\RouteCollection;
use \Symfony\Component\Routing\RequestContext;
use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\Routing\Matcher\UrlMatcher;
use \Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\HttpFoundation\Response;
use \Symfony\Component\HttpFoundation\JsonResponse;
use \Symfony\Component\Routing\Exception\MethodNotAllowedException;

$configRoutes = require_once __DIR__ . '/config/routes.php';

$excludeKeys = ['_controller', '_route'];

try {
    $routes = new RouteCollection();

    foreach ($configRoutes as $params) {
        $path = $params['pattern'];

        $defaults = [
            '_controller' => [$params['controller'], $params['action']],
        ];

        $router = new Route($path, $defaults);

        if(isset($params['requirements'])) {
            $requirements = [
                'parameter' => $params['requirements'],
            ];

            $router = new Route($path, $defaults, $requirements, [], '', [], $params['method'], '');
        }

        $routes->add($params['name'], $router);
    }

} catch (\Exception $e) {

}

try {
    $request = Request::createFromGlobals();
    $context = new RequestContext();
    $context->fromRequest(Request::createFromGlobals());

    $matcher = new UrlMatcher($routes, $context);
    $parameters = $matcher->match($context->getPathInfo());

    list($controller, $action) = $parameters['_controller'];

    $arguments = [$request];
    foreach ($parameters as $key => $parameter) {
        if (!in_array($key, $excludeKeys)) {
            $arguments [] = $parameter;
        }
    }

    $controller = new $controller();
    $response = call_user_func_array([$controller, $action], $arguments);

    if (!($response instanceof Response) && !($response instanceof JsonResponse)) {

        if(is_string($response)) {
            $response = new Response($response);
        }

        if(is_array($response)) {
            $response = new JsonResponse($response, Response::HTTP_OK, ['Content-Type' => 'application/json']);
        }
    }

} catch (ResourceNotFoundException $e) {
    $response = new Response('Not Found', Response::HTTP_BAD_REQUEST);
}
catch (MethodNotAllowedException $e) {
    $response = new Response(
        'Not allowed method. Valid methods: ' . implode(', ', $e->getAllowedMethods()),
        Response::HTTP_BAD_REQUEST
    );
}
catch (Exception | VideoProviderException $e) {
    $response = new Response($e->getMessage(), $e->getCode());
}

$response->send();
