<?php


namespace App\services\video;


use App\Exceptions\VideoProviderException;
use Symfony\Component\HttpFoundation\Response;

class AbstractVideoProvider implements VideoInterface
{
    protected $videoId;

    public function __construct(string $videoId)
    {
        $this->videoId = $videoId;
    }

    public function getInfo()
    {
        return ['videoId' => $this->videoId];
    }

    protected function request($link)
    {
        $curl = curl_init();

        if(!$curl) {
            throw new VideoProviderException('Failed to initialize curl', Response::HTTP_BAD_REQUEST);
        }
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }
}
