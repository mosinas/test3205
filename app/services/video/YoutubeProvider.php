<?php


namespace App\services\video;


use App\Exceptions\VideoProviderException;
use Symfony\Component\HttpFoundation\Response;

class YoutubeProvider extends AbstractVideoProvider
{
    protected $sourceLink = 'http://www.youtube.com/oembed?url=http%3A//youtube.com/watch%3Fv%3D{VIDEO_ID}&format=json';

   /**
    * @return array
    * @throws VideoProviderException
    */
    public function getInfo()
    {
        $link = str_replace('{VIDEO_ID}', $this->videoId, $this->sourceLink);
        return array_merge(parent::getInfo(), json_decode($this->request($link), true));
    }
}
