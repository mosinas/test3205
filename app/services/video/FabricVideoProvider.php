<?php


namespace App\services\video;


use App\Exceptions\VideoProviderException;
use Symfony\Component\HttpFoundation\Response;

class FabricVideoProvider
{
    protected $mapProviders = [
        'youtube.com' => '%ns%\\YoutubeProvider',
        'youtu.be' => '%ns%\\YoutubeProvider',
        'rutube.ru' => '%ns%\\RutubeProvider',
    ];

    public function getVideoProvider(string $url) : VideoInterface
    {
        if(
            !preg_match('/youtube.com\/watch\b\?v=([a-zA-Z0-9\-_]{11})/', $url, $matches)
            && !preg_match('/youtu.be\/([a-zA-Z0-9\-_]{11})$/', $url, $matches)
        ) {
            throw new VideoProviderException('Invalid link', Response::HTTP_BAD_REQUEST);
        }

        $videoId = $matches[1];

        $urlInfo = parse_url($url);

        if(!$urlInfo) {
            throw new VideoProviderException('Could not recognize link', Response::HTTP_BAD_REQUEST);
        }

        $host =  $urlInfo['host'];
        $host = str_replace('www.', '', $host);

        if(!isset($this->mapProviders[$host])) {
            throw new VideoProviderException('No processing provided', Response::HTTP_BAD_REQUEST);
        }

        $className = str_replace('%ns%', __NAMESPACE__, $this->mapProviders[$host]);

        $instance = new $className($videoId);

        return  $instance;

    }
}
